本仓库为 <https://github.com/saveweb/rss-list> & <https://saveweb.othing.xyz> 对应的维护仓库。

每个博客事件请单独发一个issue，格式暂时参考 [issues#4](https://github.com/saveweb/doing/issues/4) 。\
博客ID可前往box.othing.xyz获取，如：<https://box.othing.xyz/i/?get=f_1> 的ID是1，<https://box.othing.xyz/i/?get=f_410> 的ID是410。\
对于没有ID的博客，说明即可。

如果博客有对应的上游list，如 <https://github.com/timqian/chinese-independent-blogs> ，还需向上游提交pr同步更改。

---

Save The Web Project 目前的维护流程 —— 2021-06-20

## I 寻找问题内容

1. 简单的找出 RSS 损坏，RSS 失效，网站证书错误，网站域名问题等问题方向
2. 使用测试[工具](https://github.com/saveweb/tools)进行问题诊断
3. 记录问题

## II 尝试寻找问题原因

1. 查阅博文，寻找更换域名、博客系统之类的内容
2. 寻找博主的社交网络，比如推特，查看博主是否知道网站问题

## III 判断是否与博主联系

参考博文内容与社交网络中的博主意愿，来选择帮助博主的方式

+ 提醒博主网站错误
+ 给博主推荐 [Save The Web Project](https://saveweb.othing.xyz/)

## IV 联系博主

1. 获取网站关于页面以及其他信息中的联系方式
2. 以 Save The Web Project 的身份联系博主

## V 等待

(处理其他的网站)

## VI 最后

1. 检查是否有上游目录需要提交修改
2. 修改 box.othing.xyz 里的分类或其他信息
